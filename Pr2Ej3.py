# -*- coding: utf-8 -*-


# Importo las funciones de turtle

from turtle import *

tortuga = Turtle()
pantalla = Screen()

#Pido los datos por pantalla
lados=0
while(lados<3 or lados>6):
    lados = int(input('Cuantos lados tiene el poligono'))
longitud = int(input('Que longitud tiene el lado'))

# Dibujo el poligono

angulo = 360/lados

for i in range(lados):
    tortuga.forward(longitud)
    tortuga.left(angulo)
